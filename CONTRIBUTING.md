How To Contribute
=================

Contributions are very welcome.

Please read `How To Contribute <https://github.com/bharatmooc/bharatmooc-platform/blob/master/CONTRIBUTING.rst>`_ for details.

Even though it was written with ``bharatmooc-platform`` in mind, the guidelines
should be followed for Open BharatMOOC code in general.
